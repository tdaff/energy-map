#!/bin/sh

# stop activate failing with missing PS1
export PS1=${PS1:-}

set -euxo pipefail

PACKAGE="${1}"
APP="${2}"
DEPLOY_DIR="$(date +%Y%m%dT%H%M%S)"
DEPLOY_PATH="apps/${APP}/${DEPLOY_DIR}"

mkdir -p "${DEPLOY_PATH}"
tar xvzf "apps/${APP}/${PACKAGE}" -C "${DEPLOY_PATH}"

virtualenv "${DEPLOY_PATH}/venv" -p $(which python3)
source "${DEPLOY_PATH}/venv/bin/activate"
pip install --upgrade pip
pip install -r "${DEPLOY_PATH}/requirements.txt"

cat > "apps/${APP}.ini" <<EOF
[uwsgi]
app_dir = %n
app_name = energymap
virtualenv = %(apps_path)/%(app_dir)/production/venv
chdir = %(apps_path)/%(app_dir)/production
wsgi-file = energymap/wsgi.py
static-map = /static=%(apps_path)/%(app_dir)/production/prod-static
EOF

cat > "${DEPLOY_PATH}/energymap/prod_settings.py" << EOF
import os
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = "$(openssl rand -base64 32)"
DEBUG = False
ALLOWED_HOSTS = [".staging.codeshare.phy.cam.ac.uk"]
STATIC_ROOT = BASE_DIR + "/prod-static"
EOF

# Migrate previous live database
find "apps/${APP}/production/" -maxdepth 1 -name "*.sqlite3" -exec cp -v "{}" "${DEPLOY_PATH}" \;

# Still in the virtualenv
python "${DEPLOY_PATH}/manage.py" migrate
python "${DEPLOY_PATH}/manage.py" collectstatic

# Switch to live
ln -sfT "${DEPLOY_DIR}" "apps/${APP}/production"

# Clean up oldest directories
ls -dt apps/${APP}/*T* | tail -n +8 | xargs -I "{}" rm -rf "{}"