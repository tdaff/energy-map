from django.contrib import admin

from .models import School, WindReading, SolarReading

admin.site.register(School)
admin.site.register(WindReading)
admin.site.register(SolarReading)
