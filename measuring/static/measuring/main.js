// Energy measurement application

/**
 * UTILITY FUNCTIONS
 */

/**
 * Wrap text at a given width
 * @param text {string} The text to wrap
 * @param width {number} Maximum width of a line, in characters
 * @param lineSep {string} Separator to insert between lines
 * @returns {string} Joined sting with separators
 */
const wrap = function(text, width, lineSep) {
  if (text.length > width) {
    let lines = [];
    let currentLine = "";
    text.split(/\s/).map(function(value) {
      if (currentLine.length + value.length > width) {
        lines.push(currentLine + lineSep);
        currentLine = value;
      } else if (currentLine.length > 0) {
        currentLine += " " + value;
      } else {
        currentLine += value;
      }
    });
    return lines.join("") + currentLine;
  } else {
    return text;
  }
};

/**
 * UNIT CONVERSION -- SOLAR
 */

/**
 * Convert a wind reading taken in lux to Watts.
 * Size of solar panel is implicit in the calculation.
 *
 * @param {number} lux Raw value in lux
 * @returns {number} Value converted to Watts
 */
const luxToWatt = function(lux) {
  return 4.324 * lux;
};

/**
 * Convert a wind reading taken in lux to kWh
 *
 * @param {number} lux Raw value in lux
 * @returns {number} Value converted to kWh in a day
 */
const luxTokWh = function(lux) {
  return (7.64 * luxToWatt(lux)) / 1000;
};

/**
 * UNIT CONVERSION -- WIND
 */

/**
 * Interpolation function for a monotonic spline function.
 * A bit smoother than direct interpolation of points.
 *
 * Original from:
 * http://blog.mackerron.com/2011/01/01/javascript-cubic-splines/
 */
const MonotonicCubicSpline = (function() {
  function MonotonicCubicSpline(points) {
    let i, to_fix, _i, _j, _len, _len2, _ref, _ref2, _ref3, _ref4;
    let delta = [];
    let m = [];
    let alpha = [];
    let beta = [];
    let dist = [];
    let tau = [];
    let n = points.length;
    let x = points.map(function(p) {
      return p[0];
    });
    let y = points.map(function(p) {
      return p[1];
    });
    for (
      i = 0, _ref = n - 1;
      0 <= _ref ? i < _ref : i > _ref;
      0 <= _ref ? (i += 1) : (i -= 1)
    ) {
      delta[i] = (y[i + 1] - y[i]) / (x[i + 1] - x[i]);
      if (i > 0) {
        m[i] = (delta[i - 1] + delta[i]) / 2;
      }
    }
    m[0] = delta[0];
    m[n - 1] = delta[n - 2];
    to_fix = [];
    for (
      i = 0, _ref2 = n - 1;
      0 <= _ref2 ? i < _ref2 : i > _ref2;
      0 <= _ref2 ? (i += 1) : (i -= 1)
    ) {
      if (delta[i] === 0) {
        to_fix.push(i);
      }
    }
    for (_i = 0, _len = to_fix.length; _i < _len; _i++) {
      i = to_fix[_i];
      m[i] = m[i + 1] = 0;
    }
    for (
      i = 0, _ref3 = n - 1;
      0 <= _ref3 ? i < _ref3 : i > _ref3;
      0 <= _ref3 ? (i += 1) : (i -= 1)
    ) {
      alpha[i] = m[i] / delta[i];
      beta[i] = m[i + 1] / delta[i];
      dist[i] = Math.pow(alpha[i], 2) + Math.pow(beta[i], 2);
      tau[i] = 3 / Math.sqrt(dist[i]);
    }
    to_fix = [];
    for (
      i = 0, _ref4 = n - 1;
      0 <= _ref4 ? i < _ref4 : i > _ref4;
      0 <= _ref4 ? (i += 1) : (i -= 1)
    ) {
      if (dist[i] > 9) {
        to_fix.push(i);
      }
    }
    for (_j = 0, _len2 = to_fix.length; _j < _len2; _j++) {
      i = to_fix[_j];
      m[i] = tau[i] * alpha[i] * delta[i];
      m[i + 1] = tau[i] * beta[i] * delta[i];
    }
    this.x = x.slice(0, n);
    this.y = y.slice(0, n);
    this.m = m;
  }
  MonotonicCubicSpline.prototype.interpolate = function(x) {
    let h, h00, h01, h10, h11, i, t, t2, t3, y, _ref;
    for (
      i = _ref = this.x.length - 2;
      _ref <= 0 ? i <= 0 : i >= 0;
      _ref <= 0 ? (i += 1) : (i -= 1)
    ) {
      if (this.x[i] <= x) {
        break;
      }
    }
    h = this.x[i + 1] - this.x[i];
    t = (x - this.x[i]) / h;
    t2 = Math.pow(t, 2);
    t3 = Math.pow(t, 3);
    h00 = 2 * t3 - 3 * t2 + 1;
    h10 = t3 - 2 * t2 + t;
    h01 = -2 * t3 + 3 * t2;
    h11 = t3 - t2;
    y =
      h00 * this.y[i] +
      h10 * h * this.m[i] +
      h01 * this.y[i + 1] +
      h11 * h * this.m[i + 1];
    return y;
  };
  return MonotonicCubicSpline;
})();

const windConversionTable = [
  [0.0, 0.0],
  [0.459, 0.05],
  [0.917, 0.05],
  [1.376, 0.25],
  [1.835, 0.55],
  [2.294, 1],
  [2.752, 1.7],
  [3.211, 2.75],
  [3.67, 3.7],
  [4.128, 4.75],
  [4.587, 5.8],
  [5.046, 6.9],
  [5.505, 7.7],
  [5.963, 8],
  [6.422, 8],
  [6.881, 8],
  [7.339, 7.8],
  [7.798, 7.7],
  [8.257, 7.6],
  [8.716, 7.5],
  [9.174, 7.7],
  [9.633, 7.6],
  [10.092, 7.6],
  [10.55, 7.6],
  [11.009, 7.6],
  [11.468, 7.6],
  [11.927, 7.6],
  [12.385, 7.6],
  [12.844, 7.6],
  [13.303, 7.6],
  [13.761, 7.6],
  [14.22, 0]
];

const windConverter = new MonotonicCubicSpline(windConversionTable);

/**
 * Convert a raw wind speed reading to an energy in Watts.
 * Conversion is calibrated for a 15m high turbine.
 *
 * @param windSpeed
 * @returns {number} power in Watts
 */
const windSpeedToWatt = function(windSpeed) {
  if (windSpeed > 13.761) {
    return 0;
  } else {
    return 1000 * windConverter.interpolate(windSpeed);
  }
};

const windSpeedTokWh = function(windSpeed) {
  return (24 * windSpeedToWatt(windSpeed)) / 1000;
};

/**
 * Format an number with an exponent if required. Exponents are
 * converted to superscript notation.
 *
 * @param {number} num
 * @returns {*}
 */
const formatExponent = function(num) {
  if (num === 0) {
    return "0";
  } else if (Math.abs(num) < 0.001) {
    return num.toExponential(3).replace("e", "×10<sup>") + "</sup>";
  } else if (Math.abs(num) > 99999) {
    return num.toExponential(3).replace("e", "×10<sup>") + "</sup>";
  } else {
    return num
      .toFixed(3)
      .replace(/0+$/g, "")
      .replace(/\.$/g, "");
  }
};

/*
 * Plotly customisations
 */
// Consistent look for plots
const standardLayout = {
  // Transparent on the background
  paper_bgcolor: "rgba(0, 0, 0, 0)",
  plot_bgcolor: "white",
  // Draw a box around the plot using mirror
  xaxis: {
    linecolor: "black",
    linewidth: 1,
    mirror: true
  },
  yaxis: {
    linecolor: "black",
    linewidth: 1,
    mirror: true
  },
  // Uses most space
  margin: {
    l: 40,
    r: 10,
    b: 40,
    t: 5
  },
  // Above the plot
  legend: {
    x: 0,
    y: 1.2
  }
};

// Consistent behaviour of plots
const standardConfig = {
  // Resize when the page shape changes
  responsive: true
};

/*
 * Background images from unsplash
 */
const backgroundImages = [
  // https://unsplash.com/photos/V4ZYJZJ3W4M
  // blue solar panel boards
  "1521618755572-156ae0cdd74d",
  // https://unsplash.com/photos/Ilpf2eUPpUE
  // aerial photography of grass field with blue solar panels
  "1497435334941-8c899ee9e8e9",
  // https://unsplash.com/photos/ZO7UsokVH98
  // Solar panels on paving slabs
  "1544245607-99ac92ec1da5",
  // https://unsplash.com/photos/eIBTh5DXW9w
  // Windmills on a field
  "1508791290064-c27cc1ef7a9a",
  // https://unsplash.com/photos/s-3EZB0EsXI
  // Full page windmill
  "1538309639218-3432f79e0894",
  // https://unsplash.com/photos/0w-uTa0Xz7w
  // windmills on a field at golden hour
  "1466611653911-95081537e5b7"
];

/*
 * On page functionality
 */
// Add interactivity once page is loaded
$(function() {
  // Clear messages on click
  $(".message").on("click", function() {
    $(this).remove();
  });

  // Click confirmations
  $(".confirm-click").on("click", function(event) {
    if (confirm("Are you sure?")) {
      return true;
    } else {
      event.stopPropagation();
      event.preventDefault();
      return false;
    }
  });

  // Activate tooltips
  $('[data-toggle="tooltip"]').tooltip();
  $('[href="#"]').on("click", function () {return false});

  // Set a background image to a random one
  // from unsplash.
  // Set through the css variable for the background image on the
  // background image elements.
  $(".bg-image-random").each(function() {
    let randomImage =
      backgroundImages[Math.floor(Math.random() * backgroundImages.length)];
    let imgSrc =
      "url('https://images.unsplash.com/photo-" +
      randomImage +
      "?fm=jpg&q=70&w=1080')";
    $(this).css("--bg-image", imgSrc);
  });

  /*
   * Table unit conversion
   */
  let convertTableUnits = function(units) {
    let solarConverter,
      windConverter,
      solarText,
      windText,
      solarDigits,
      windDigits;
    if (units === "kwh") {
      solarConverter = luxTokWh;
      windConverter = windSpeedTokWh;
      solarText = "kWh";
      windText = "kWh";
      solarDigits = 3;
      windDigits = 3;
    } else if (units === "raw") {
      solarConverter = function(x) {
        return Number(x);
      };
      windConverter = function(x) {
        return Number(x);
      };
      solarText = "lux";
      windText = "m/s";
      solarDigits = 1;
      windDigits = 2;
    } else {
      solarConverter = luxToWatt;
      windConverter = windSpeedToWatt;
      solarText = "Watt";
      windText = "Watt";
      solarDigits = 1;
      windDigits = 1;
    }
    // Solar
    $(".readings-solar").each(function() {
      $(".reading-units", this).each(function() {
        $(this).text(solarText);
      });
      $("td[data-reading]", this).each(function() {
        let elem = $(this);
        let reading = elem.data("reading");
        if (reading !== "") {
          elem.text(solarConverter(reading).toFixed(solarDigits));
        }
      });
    });
    // Wind
    $(".readings-wind").each(function() {
      $(".reading-units", this).each(function() {
        $(this).text(windText);
      });
      $("td[data-reading]", this).each(function() {
        let elem = $(this);
        let reading = elem.data("reading");
        if (reading !== "") {
          elem.text(windConverter(elem.data("reading")).toFixed(windDigits));
        }
      });
    });
  };

  /**
   * Build or rebuild plots
   */
  let renderPlots = function(units) {
    let solarConverter, windConverter, solarText, windText;
    if (units === "kwh") {
      solarConverter = luxTokWh;
      windConverter = windSpeedTokWh;
      solarText = "kWh";
      windText = "kWh";
    } else if (units === "raw") {
      solarConverter = function(x) {
        return Number(x);
      };
      windConverter = function(x) {
        return Number(x);
      };
      solarText = "lux";
      windText = "m/s";
    } else {
      solarConverter = luxToWatt;
      windConverter = windSpeedToWatt;
      solarText = "Watt";
      windText = "Watt";
    }

    // Solar plots
    $(".solar-plot").each(function() {
      // Data stored in a contained script
      let data = JSON.parse($("script", this).text());
      // Convert as required
      data.y = data.y.map(solarConverter);
      // Add units to the axis
      let layout = Object.create(standardLayout);
      layout.yaxis.title = { text: solarText };
      // Add the plot visual customisations
      Object.assign(data, {
        type: "scatter",
        mode: "lines+markers",
        line: { width: 4, color: "#d4c23a" },
        marker: { size: 10, line: { width: 2 } }
      });
      // .react messes up page flow, newPlot every time
      Plotly.newPlot($(".plot-area", this)[0], [data], layout, standardConfig);
    });
    // Wind plots
    $(".wind-plot").each(function() {
      // Data stored in a contained script
      let data = JSON.parse($("script", this).text());
      // Convert as required
      data.y = data.y.map(windConverter);
      // Add units to the axis
      let layout = Object.create(standardLayout);
      layout.yaxis.title = { text: windText };
      // Add the plot visual customisations
      Object.assign(data, {
        type: "scatter",
        mode: "lines+markers",
        line: { width: 4, color: "#2e5ec5" },
        marker: { size: 10, line: { width: 2 } }
      });
      // .react messes up page flow, newPlot every time
      Plotly.newPlot($(".plot-area", this)[0], [data], layout, standardConfig);
    });
    // Multiple lines solar plot
    $(".solar-plot-multi").each(function() {
      // Data stored in a contained script
      let data = JSON.parse($("script", this).text());
      // Add units to the axis
      let layout = Object.create(standardLayout);
      layout.yaxis.title = { text: solarText };
      // Multiple lines to plot
      let plotData = [];
      // Iterate over each school, alphabetical order
      Object.keys(data)
        .sort()
        .forEach(function(school) {
          // Convert as required
          data[school].y = data[school].y.map(solarConverter);
          plotData.push(
            Object.assign(data[school], {
              name: school,
              type: "scatter",
              mode: "lines+markers",
              line: { width: 4 },
              marker: { size: 10, line: { width: 2 } }
            })
          );
        });
      // .react messes up page flow, newPlot every time
      Plotly.newPlot(
        $(".plot-area", this)[0],
        plotData,
        layout,
        standardConfig
      );
    });
    // Multiple lines wind plot
    $(".wind-plot-multi").each(function() {
      // Data stored in a contained script
      let data = JSON.parse($("script", this).text());
      // Add units to the axis
      let layout = Object.create(standardLayout);
      layout.yaxis.title = { text: windText };
      // Multiple lines to plot
      let plotData = [];
      // Iterate over each school, alphabetical order
      Object.keys(data)
        .sort()
        .forEach(function(school) {
          // Convert as required
          data[school].y = data[school].y.map(windConverter);
          plotData.push(
            Object.assign(data[school], {
              name: school,
              type: "scatter",
              mode: "lines+markers",
              line: { width: 4 },
              marker: { size: 10, line: { width: 2 } }
            })
          );
        });
      // .react messes up page flow, newPlot every time
      Plotly.newPlot(
        $(".plot-area", this)[0],
        plotData,
        layout,
        standardConfig
      );
    });
    // Solar plot with cumulative total
    $(".solar-plot-cumulative").each(function() {
      // Data stored in a contained script
      let data = JSON.parse($("script", this).text());
      // Add units to the axis, always the same
      let layout = Object.create(standardLayout);
      layout.yaxis.title = { text: "kWh total" };
      // Multiple lines to plot
      let plotData = [];
      // Iterate over each school, alphabetical order
      Object.keys(data)
        .sort()
        .forEach(function(school) {
          // Cumulative sum for after conversion to kWh
          let cumulativeY = [];
          // Calculate the running total, always in kWh
          data[school].y.reduce(function(acc, cur, idx) {
            return (cumulativeY[idx] = acc + luxTokWh(cur));
          }, 0);
          data[school].y = cumulativeY;
          plotData.push(
            Object.assign(data[school], {
              name: school,
              type: "scatter",
              mode: "lines+markers",
              line: { width: 4 },
              marker: { size: 10, line: { width: 2 } }
            })
          );
        });
      // .react messes up page flow, newPlot every time
      Plotly.newPlot(
        $(".plot-area", this)[0],
        plotData,
        layout,
        standardConfig
      );
    });
    // Solar plot with cumulative total
    $(".wind-plot-cumulative").each(function() {
      // Data stored in a contained script
      let data = JSON.parse($("script", this).text());
      // Cumulative sum for after conversion to kWh
      let cumulativeY = [];
      // Add units to the axis, always the same
      let layout = Object.create(standardLayout);
      layout.yaxis.title = { text: "kWh total" };
      // Multiple lines to plot
      let plotData = [];
      // Iterate over each school, alphabetical order
      Object.keys(data)
        .sort()
        .forEach(function(school) {
          // Calculate the running total, always in kWh
          data[school].y.reduce(function(acc, cur, idx) {
            return (cumulativeY[idx] = acc + windSpeedTokWh(cur));
          }, 0);
          data[school].y = cumulativeY;
          plotData.push(
            Object.assign(data[school], {
              name: school,
              type: "scatter",
              mode: "lines+markers",
              line: { width: 4 },
              marker: { size: 10, line: { width: 2 } }
            })
          );
        });
      // .react messes up page flow, newPlot every time
      Plotly.newPlot(
        $(".plot-area", this)[0],
        plotData,
        layout,
        standardConfig
      );
    });
  };

  // Unit switcher, set units for the page and render plots
  let cachedUnits = localStorage.getItem("units") || "watt";
  let unitSwitch = $("#unit-switch");
  unitSwitch.val(cachedUnits);
  unitSwitch
    .on("change", function() {
      let newUnits = $(this).val();
      localStorage.setItem("units", newUnits);
      convertTableUnits(newUnits);
      renderPlots(newUnits);
    })
    .trigger("change");

  // On the fly unit conversion
  $("#measurement-value")
    .on("input change", function() {
      let elem = $(this);
      let value = elem.val();
      let form = elem.closest("form");
      let measurementType = form.find("#measurement-type").val();
      let output = form.find("#converted-value");
      // Converter depends on selected input
      if (measurementType === "solar") {
        output.text(luxToWatt(value).toFixed(1) + " Watt");
      } else if (measurementType === "wind") {
        if (value > 13.761) {
          output.text("0.0 Watt! Turbine cut-out!");
        } else {
          output.text(windSpeedToWatt(value).toFixed(1) + " Watt");
        }
      } else {
        output.text("");
      }
    })
    .trigger("change");

  // Recalculate the converted energy when type changes
  $("#measurement-type").on("change", function() {
    $(this)
      .closest("form")
      .find("#measurement-value")
      .trigger("change");
  });

  // Unit conversion widget
  $("#unit-conversion-widget")
    .on("change input", function() {
      let power = $("#input-power").val();
      let powerFactor = $("#input-power-units").val();
      let time = $("#input-time").val();
      let timeFactor = $("#input-time-units").val();
      let joules = power * powerFactor * time * timeFactor;
      let kilojoules = joules / 1000;
      let kwh = joules / 3600000;
      $("#conversion-result").html(
        "<p>" +
          formatExponent(joules) +
          " Joules </p>" +
          "<p>" +
          formatExponent(kilojoules) +
          " Kilojoules </p>" +
          "<p>" +
          formatExponent(kwh) +
          " kWh</p>"
      );
    })
    .trigger("change");

  // Make a map for a single location in any location-map element
  $(".location-map").each(function() {
    let map = $(this);
    let data = [
      {
        type: "scattergeo",
        mode: "markers+text",
        lon: [map.data("lon")],
        lat: [map.data("lat")],
        text: [wrap(map.data("name"), 16, "<br>")],
        textposition: ["left"],
        marker: {
          size: 10,
          color: "#ffffff",
          line: {
            width: 2
          }
        }
      }
    ];
    let layout = {
      paper_bgcolor: "rgba(0, 0, 0, 0)",
      width: 300,
      height: 400,
      margin: {
        l: 0,
        r: 0,
        b: 0,
        t: 0
      },
      font: {
        size: 16,
        color: "#ffffff"
      },
      geo: {
        scope: "europe",
        lonaxis: {
          range: [-10, 2]
        },
        lataxis: {
          range: [50, 59]
        },
        resolution: 50,
        showland: true,
        landcolor: "#88aa88",
        showocean: true,
        countrywidth: 2
      }
    };
    Plotly.newPlot(this, data, layout);
  });
});
