from django.urls import path
from django.views.generic import TemplateView

from . import views


def template(src):
    return TemplateView.as_view(template_name=src)


app_name = "measuring"
urlpatterns = [
    path("data", views.data, name="data"),
    path("archives", views.archives, name="archives"),
    path("school/<str:school_name>/", views.school_view, name="school"),
    path("school/<str:school_name>/submit/", views.submit_measurement, name="submit"),
    path("school/<str:school_name>/edit/", views.edit_school, name="edit"),
    path("school/<str:school_name>/archive/", views.archive, name="archive"),
    path("<str:reading_type>_reading/<int:reading_id>", views.reading, name="reading"),
    path("login", views.user_login, name="login"),
    path("admin", views.admin, name="admin"),
    path("logout", views.user_logout, name="logout"),
    path("", template("measuring/info.html"), name="index"),
]
