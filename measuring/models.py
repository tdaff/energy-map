from datetime import datetime as dt
from collections import OrderedDict

from django.db import models
from django.utils.timezone import get_current_timezone


class School(models.Model):
    """Each School collects their own data"""

    def __str__(self):
        return self.name

    # School is weakly linked to the User that is used for login
    # by the username field. New Schools will be created automatically
    # when a new non-admin user is created and used.
    username = models.CharField("school short name", max_length=30, primary_key=True)
    name = models.CharField("Full name of the school", max_length=255)
    # Location data for mapping. Defaults to Cavendish Lab
    latitude = models.FloatField(default=52.209132)
    longitude = models.FloatField(default=0.092965)
    elevation = models.FloatField(default=0.0)
    # Archived data is not shown on the main page
    archived = models.BooleanField(default=False)


class Reading(models.Model):
    """A generic series of timed readings"""

    SUNNY = "sunny"
    PARTLY_SUNNY = "partly_sunny"
    CLOUDY = "cloudy"
    RAINY = "rain"
    WEATHER_CHOICES = OrderedDict(
        (
            (SUNNY, "Sunny"),
            (PARTLY_SUNNY, "Partly sunny"),
            (CLOUDY, "Cloudy"),
            (RAINY, "Rainy"),
        )
    )
    WEATHER_SYMBOLS = {
        SUNNY: "fas fa-sun fa-fw",
        PARTLY_SUNNY: "fas fa-cloud-sun fa-fw",
        CLOUDY: "fas fa-cloud fa-fw",
        RAINY: "fas fa-cloud-rain fa-fw",
    }

    class Meta:
        abstract = True
        ordering = ["-datetime"]

    # Proper relationship with the school
    school = models.ForeignKey(School, on_delete=models.CASCADE)
    datetime = models.DateTimeField()
    value = models.FloatField(default=0.0)
    weather = models.CharField(max_length=12, choices=WEATHER_CHOICES.items())

    @property
    def date(self) -> dt.date:
        """Date part of datetime, in configured timezone"""
        return self.datetime.astimezone(get_current_timezone()).date()

    @property
    def time(self) -> dt.time:
        """Time part of datetime, in configured timezone"""
        return self.datetime.astimezone(get_current_timezone()).time()

    @property
    def form_date(self) -> str:
        """Date part of datetime suitable for a form value"""
        return self.datetime.astimezone(get_current_timezone()).strftime("%Y-%m-%d")

    @property
    def form_time(self) -> str:
        """Time part of datetime suitable for a form value"""
        return self.datetime.astimezone(get_current_timezone()).strftime("%H:%M")

    @property
    def weather_symbol(self) -> str:
        """The fontawesome symbol corresponding to the weather."""
        return self.WEATHER_SYMBOLS[self.weather]


class SolarReading(Reading):
    """Data pints for solar intensity measurements"""

    def __str__(self):
        return "solar, {}, {}".format(self.datetime, self.value)


class WindReading(Reading):
    """Data points for wind speed measurements"""

    def __str__(self):
        return "wind, {}, {}".format(self.datetime, self.value)
