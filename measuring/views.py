from itertools import count

from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.messages import add_message, ERROR, SUCCESS
from django.http import HttpRequest, HttpResponse, Http404
from django.utils.timezone import make_aware
from django.utils.dateparse import parse_datetime
from django.shortcuts import render, redirect, get_object_or_404

from .data import to_list, to_xy, to_xy_sets
from .models import School, SolarReading, WindReading


def data(request: HttpRequest) -> HttpResponse:
    """Landing page that shows data and all operations."""
    # Overview page will show all the data for all schools at once
    # with the ability to interact with the entire dataset.
    solar_sets = {
        school.name: school.solarreading_set
        for school in School.objects.exclude(archived=True)
    }
    solar_xy = to_xy_sets(solar_sets)
    wind_sets = {
        school.name: school.windreading_set
        for school in School.objects.exclude(archived=True)
    }
    wind_xy = to_xy_sets(wind_sets)
    context = {
        "data": {"wind": {"xy": wind_xy}, "solar": {"xy": solar_xy}},
        "meta": School.objects.exclude(archived=True).all(),
    }
    return render(request, "measuring/data.html", context)


def archives(request: HttpRequest) -> HttpResponse:
    """Display past data in a similar page to the index."""
    solar_sets = {
        school.name: school.solarreading_set
        for school in School.objects.filter(archived=True)
    }
    solar_xy = to_xy_sets(solar_sets)
    wind_sets = {
        school.name: school.windreading_set
        for school in School.objects.filter(archived=True)
    }
    wind_xy = to_xy_sets(wind_sets)
    context = {
        "data": {"wind": {"xy": wind_xy}, "solar": {"xy": solar_xy}},
        "meta": School.objects.filter(archived=True).all(),
        "archives": True,
    }
    return render(request, "measuring/data.html", context)


def user_login(request: HttpRequest) -> HttpResponse:
    """Login page allows user to select a school from a dropdown to login."""
    if request.method == "POST":
        # attempt to login using the submitted form data
        try:
            # Empty values raise an error
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(username=username, password=password)

            # Not able to authenticate, or disabled account
            if user is None or not user.is_active:
                raise ValueError

            # Begin the user's session
            login(request, user)

            # Send admins to the admin interface
            if user.is_staff:
                return redirect("measuring:admin")

            # User is not staff, so must be a school. If this is the first
            # time logging in, ensure a School is created
            School.objects.get_or_create(pk=username, defaults={"name": user.last_name})
            # Logged in for the index page
            return redirect("measuring:school", school_name=username)

        except (KeyError, ValueError):
            # KeyError for missing fields
            # ValueError for invalid credentials
            add_message(request, ERROR, "Invalid username or password")
            return redirect("measuring:login")

    # Not trying to login, so present the login page instead

    # Login options are the school users which are all non-admins
    # in the system
    schools = (
        get_user_model()
        .objects.filter(is_active=True, is_staff=False)
        .order_by("last_name")
        .all()
    )

    context = {"schools": schools}
    return render(request, "measuring/login.html", context)


def user_logout(request: HttpRequest) -> HttpResponse:
    """Clear any login data and return to index"""
    logout(request)
    return redirect("measuring:index")


def school_view(request, school_name: str) -> HttpResponse:
    """Data for a single school, with more details of the school"""
    school = get_object_or_404(School, pk=school_name)
    context = {
        "school_name": school_name,
        "data": {
            "meta": school,
            "wind": to_list(school.windreading_set),
            "solar": to_list(school.solarreading_set),
            "wind_xy": to_xy(school.windreading_set),
            "solar_xy": to_xy(school.solarreading_set),
        },
    }
    return render(request, "measuring/school.html", context)


def submit_measurement(request, school_name: str) -> HttpResponse:
    """Record a new reading for a measurement at the selected school"""
    school = get_object_or_404(School, pk=school_name)

    # Validate if the user can add data. Only the school itself
    # or an admin can update
    if request.user.username != school_name and not request.user.is_staff:
        add_message(request, ERROR, "Must be logged in to record a measurement")
        return redirect("measuring:login")

    # Creation of the data point will raise an error if the form
    # has submitted bad data and then we can follow the error pathway
    try:
        # Choose the right set of measurements to append to
        if request.POST["type"] == "wind":
            dataset = school.windreading_set
        elif request.POST["type"] == "solar":
            dataset = school.solarreading_set
        else:
            raise ValueError

        date_string = "{dt[date]} {dt[time]}".format(dt=request.POST)

        dataset.create(
            value=request.POST["value"],
            datetime=make_aware(parse_datetime(date_string)),
            weather=request.POST["weather"],
        )
    except (KeyError, ValueError, AttributeError):
        add_message(request, ERROR, "Invalid measurement entry!")
        return redirect("measuring:school", school_name=school_name)

    return redirect("measuring:school", school_name=school_name)


def edit_school(request, school_name: str) -> HttpResponse:
    """
    Edit details of the school.

    Allow users to update their location or any other details that might
    need to be updated manually.
    """
    school = get_object_or_404(School, pk=school_name)

    # Validate if the user can add data. Only the school itself
    # or an admin can update
    if request.user.username != school_name and not request.user.is_staff:
        add_message(request, ERROR, "Must be logged in to edit school details")
        return redirect("measuring:login")

    # Any errors will return to the same view with a message banner
    try:
        school.latitude = request.POST["latitude"]
        school.longitude = request.POST["longitude"]
        school.save()
    except (ValueError, KeyError):
        add_message(request, ERROR, "Invalid values for school details!")
        return redirect("measuring:school", school_name=school_name)

    return redirect("measuring:school", school_name=school_name)


def archive(request, school_name: str) -> HttpResponse:
    """
    Move the data into an archived version of the school.
    """
    # Only Admins can archive data
    if not request.user.is_staff:
        add_message(request, ERROR, "Action only available for admins")
        return redirect("measuring:login")

    school = get_object_or_404(School, pk=school_name)

    # Find the next consecutive archive name and create it
    for idx in count():
        archive_name = "{}_a{}".format(school_name, idx)
        if School.objects.filter(username=archive_name):
            continue
        # clone all attributes
        archive_school = School.objects.create(
            pk=archive_name,
            name=school.name,
            latitude=school.latitude,
            longitude=school.longitude,
            archived=True,
        )
        archive_school.save()
        break
    else:
        # stop ide complaints about undefined archive_school
        archive_school = School()

    # Move the data
    school.windreading_set.update(school=archive_school)
    school.solarreading_set.update(school=archive_school)

    add_message(request, SUCCESS, "Archived {}".format(school.name))
    return redirect("measuring:school", school_name=archive_school.pk)


def reading(request, reading_type: int, reading_id: str) -> HttpResponse:
    """Interact with a single measurement."""
    # Must be an existing reading
    if reading_type == "solar":
        value = get_object_or_404(SolarReading, pk=reading_id)
    elif reading_type == "wind":
        value = get_object_or_404(WindReading, pk=reading_id)
    else:
        raise Http404

    # CHeck if it's an update request
    if request.method == "POST":
        user = request.user
        school = value.school
        # Validate if the user can edit data. Only the school itself
        # or an admin can update
        if user.username != school.username and not user.is_staff:
            add_message(request, ERROR, "Must be logged in to modify a measurement")
            return redirect("measuring:reading", reading_type, reading_id)

        # If delete has been clicked then remove entry and return
        # to the school page
        if "delete" in request.POST:
            value.delete()
            add_message(request, SUCCESS, "Measurement deleted!")
            return redirect("measuring:school", school.username)

        # Update with the POSTed values
        date_string = "{dt[date]} {dt[time]}".format(dt=request.POST)
        value.value = request.POST["value"]
        value.datetime = make_aware(parse_datetime(date_string))
        value.weather = request.POST["weather"]
        value.save()
        # Carry on to default view of a value with success message
        add_message(request, SUCCESS, "Measurement updated!")

    context = {"reading_type": reading_type, "reading": value}

    return render(request, "measuring/reading.html", context=context)


def admin(request) -> HttpResponse:
    """
    General admin interface for logging in and basic management
    of the schools.

    On first run, prompt to create the admin user.
    """

    user = get_user_model()
    admins = user.objects.filter(is_active=True, is_staff=True).all()
    schools = user.objects.filter(is_active=True, is_staff=False).all()

    context = {"schools": schools}

    # There are no site admins, so we do the setup routine to create one
    if not admins:
        if request.method == "POST":
            username = request.POST["username"]
            password = request.POST["password"]
            user.objects.create_superuser(
                username=username, email=None, password=password
            )
            add_message(request, SUCCESS, "Created user: {}".format(username))
            return redirect("measuring:admin")
        else:
            return render(request, "measuring/admin.html", context={"mode": "setup"})

    # Check that user is a logged-in member of staff
    if not request.user.is_staff:
        return render(request, "measuring/admin.html", context={"mode": "login"})

    # POST requests come from any forms on the page that have been
    # submitted
    if request.method == "POST":
        # Only form is to add a new school. A school exists as
        # a user in the database
        username = request.POST["new-school-username"]
        school_name = request.POST["new-school-name"]
        password = request.POST["new-school-password"]
        user.objects.create_user(
            username=username, password=password, last_name=school_name
        )
        add_message(request, SUCCESS, "Created school: {}".format(school_name))
        return redirect("measuring:admin")

    # Nothing special is happening so just render the landing page
    return render(request, "measuring/admin.html", context=context)
