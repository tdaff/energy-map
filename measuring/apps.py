from django.apps import AppConfig


class MeasuringConfig(AppConfig):
    name = "measuring"
