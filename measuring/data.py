"""
Processing data to convert between different
views and formats for displaying graphs and
tables.

"""
from itertools import groupby
from operator import itemgetter
from typing import List, Dict, Tuple

from django.db import models


def to_list(result_set: models.QuerySet) -> List[dict]:
    """
    Reformat the set of results as a dict that can be passed
    to regroup so it can be grouped by date.
    """
    readings = [
        {
            "pk": reading.pk,
            "date": reading.date,
            "time": reading.time,
            "value": reading.value,
            "symbol": reading.weather_symbol,
        }
        for reading in result_set.all()
    ]

    return readings


def to_xy(result_set: models.QuerySet) -> Dict[str, list]:
    """
    Reformat the set of results as a series of string format dates
    for x vales, and raw values for y.

    Data is returned in oldest to newest order.
    """
    values = {"x": [], "y": []}
    for reading in result_set.all():
        values["x"].append(reading.datetime.isoformat())
        values["y"].append(reading.value)
    # Change order
    values["x"].reverse()
    values["y"].reverse()
    return values


def to_xy_sets(data: Dict[str, models.QuerySet]) -> Dict[str, Dict[str, list]]:
    """
    Reformat multiple school data into xy pairs.
    """
    values = {school: to_xy(readings) for school, readings in data.items()}
    return values


def to_table(data: Dict[str, models.QuerySet]) -> Tuple[List, List[List]]:
    """
    Reformat multiple school data as a table.

    Table format is a row for each distinct date with three cells
    allocated for each school. Cells are empty where there is no
    reading for that school on that date. If a school has more than
    one reading on a day. extra rows are added leaving the date field
    blank and cells empty where other schools have no extra readings.

    Parameters
    ----------
    data
        A dictionary with school names as the keys and a dataset
        as the values.

    """
    # Merge the results from all schools together
    readings = [
        {
            "date": reading.date,
            "school": school,
            "time": reading.time,
            "value": reading.value,
            "symbol": reading.weather_symbol,
        }
        for school, result_set in data.items()
        for reading in result_set.all()
    ]

    # Treat schools in alphabetical order
    schools = sorted(data)

    rows = []
    # Reverse date order, group into date blocks
    date_key = itemgetter("date")
    school_key = itemgetter("school")
    grouped_data = groupby(sorted(readings, key=date_key, reverse=True), key=date_key)
    for date, date_data in grouped_data:
        # Each measurement for a date on a separate row
        date_rows = []
        # Each school has a set of columns in a fixed position
        for school, school_data in groupby(date_data, key=school_key):
            school_col = 3 * schools.index(school)
            # Each row initialised as a set of blanks
            for row_id, point in enumerate(school_data):
                if row_id >= len(date_rows):
                    date_rows.append([""] * (1 + 3 * (len(schools))))
                # Expected format of tabular data
                values = [point["time"], point["value"], point["symbol"]]
                date_rows[row_id][1 + school_col : 4 + school_col] = values
        # Insert date only into first cell, blank on subsequent rows
        date_rows[0][0] = date

        rows.extend(date_rows)

    return schools, rows
