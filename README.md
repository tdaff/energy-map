# Primary School Energy Mapping Challenge

A web application for tracking the energy potentially generated from wind
and solar at given locations.

Have a look at the live site: https://energymap.oe.phy.cam.ac.uk/

Each school can input their own readings and track their results against
all the other schools!

## Technical

The software is written in Python using the Django framework.